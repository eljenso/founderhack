package com.founderhack.car2phone;

public class Card {
	
	private int Cardtype;
	private int position;
	private String text;
	private String datetime;
	
	//TripView
	private String avSpeed;
	private String avConsumption;
	private String travelTime;
	private String avSpeedText;
	private String avConsumptionText;
	
	//MapView
	private String longitude;
	private String latitude;
	
	public Card(int cardType, int position, String text){
		this.Cardtype = cardType;
		this.position = position;
		this.text = text;
	}
	
	public int getCardtype() {
		return Cardtype;
	}
	public void setCardtype(int cardtype) {
		Cardtype = cardtype;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPosition(){
		return this.position;
	}
	public void setPosition(int pos){
		this.position = pos;
	}
	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getAvSpeed() {
		return avSpeed;
	}

	public void setAvSpeed(String avSpeed) {
		this.avSpeed = avSpeed;
	}

	public String getAvConsumption() {
		return avConsumption;
	}

	public void setAvConsumption(String avConsumption) {
		this.avConsumption = avConsumption;
	}

	public String getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(String travelTime) {
		this.travelTime = travelTime;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getUrl(){
		String r = "http://maps.google.com/maps/api/staticmap?center="+this.getLongitude()+","+this.getLatitude()+"&zoom=18&size=400x200&sensor=false&visual_refresh=true";
		return r;
	}
	
	public String getAvSpeedText() {
		return avSpeedText;
	}

	public void setAvSpeedText(String avSpeedText) {
		this.avSpeedText = avSpeedText;
	}

	public String getAvConsumptionText() {
		return avConsumptionText;
	}

	public void setAvConsumptionText(String avConsumptionText) {
		this.avConsumptionText = avConsumptionText;
	}

}
