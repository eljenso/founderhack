package com.founderhack.car2phone;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.founderhack.autodata.CarData;
import com.founderhack.autodata.Drive;
import com.haarman.listviewanimations.ArrayAdapter;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.haarman.listviewanimations.itemmanipulation.SwipeDismissAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;

public class MainActivity extends Activity implements OnDismissCallback{
	
	private GoogleCardsAdapter mGoogleCardsAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Hier laden wir die Adapter, Listviews und Animationen, welche uns die View erm�glichen
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_googlecards);
		ListView listView = (ListView) findViewById(R.id.activity_googlecards_listview);
		mGoogleCardsAdapter = new GoogleCardsAdapter(this);
		SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(new SwipeDismissAdapter(mGoogleCardsAdapter, this));
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		swingBottomInAnimationAdapter.setAbsListView(listView);
		listView.setAdapter(swingBottomInAnimationAdapter);

		mGoogleCardsAdapter.addAll(getItems());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private ArrayList<Card> getItems() {
		//Wir laden ein CarData Objekt um die Messwerte des Autos dem Programm zu �bermitteln
		CarData data = new CarData();
		List<Drive> driveData =data.getListOfDrives(5);
		ArrayList<Card> items = new ArrayList<Card>();
		Drive drive1 = data.getLastDrive();
		//Hier wird die letzte gespeicherte Fahrt geladen
		Card tripInfo = new Card(1, 1, "Tripinfo");
		tripInfo.setAvConsumption(drive1.getAvarageConsumption());
		tripInfo.setAvSpeed(drive1.getAvarageSpeed());
		tripInfo.setTravelTime(drive1.getDuration());
		tripInfo.setDatetime(drive1.getStarted());
		//Hier kommt der Standort des Autos hinzu
		Card mapView = new Card(2, 2, "Map");
		mapView.setLatitude(drive1.getParkposition().getLongitude());
		mapView.setLongitude(drive1.getParkposition().getLatitude());
		items.add(tripInfo);		
		items.add(mapView);			
		
		Card cardLabel = new Card(0,1, "�ltere Fahrten");
		items.add(cardLabel);
		//Im folgenden laden wir die letzten 5 Fahrten, welche gespeichert sind
		for(Drive drive : driveData){	
			//TripInfo
			 tripInfo = new Card(1, 1, "Tripinfo");
			tripInfo.setAvConsumption(drive.getAvarageConsumption());
			tripInfo.setAvSpeed(drive.getAvarageSpeed());
			tripInfo.setTravelTime(drive.getDuration());
			tripInfo.setDatetime(drive.getStarted());
			items.add(tripInfo);
			
			//MapView Es w�re m�glich auch die Standorte mitzuladen aber man hat das Auto bekanntlich gefunden
			/*mapView = new Card(2, 2, "Map");
			mapView.setLatitude(drive.getParkposition().getLongitude());
			mapView.setLongitude(drive.getParkposition().getLatitude());		
			items.add(mapView);*/			
			
		}		
		
		return items;
	}

	@Override
	public void onDismiss(AbsListView listView, int[] reverseSortedPositions) {
		for (int position : reverseSortedPositions) {
			mGoogleCardsAdapter.remove(position);
		}
	}
	
	private static class GoogleCardsAdapter extends ArrayAdapter<Card> {
		
		private Context mContext;
		private LruCache<Integer, Bitmap> mMemoryCache;

		public GoogleCardsAdapter(Context context) {
			mContext = context;

			final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

			// Use 1/8th of the available memory for this memory cache.
			final int cacheSize = maxMemory;
			mMemoryCache = new LruCache<Integer, Bitmap>(cacheSize) {
				@Override
				protected int sizeOf(Integer key, Bitmap bitmap) {
					// The cache size will be measured in kilobytes rather than
					// number of items.
					return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
				}
			};
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			View view = convertView;
			
			Card curCard = getItem(position);
			
			if (view == null) {
				
				viewHolder = new ViewHolder();
		
				switch(curCard.getCardtype()){
					
					//LabelDivider
					case 0:
						view = LayoutInflater.from(mContext).inflate(R.layout.label_divider_card, parent, false);
						viewHolder.textView = (TextView) view.findViewById(R.id.label_divider_card_Text);
						viewHolder.textView.setText(curCard.getText());
						view.setTag(viewHolder);
						
						break;						
					
					//TripView
					case 1:
						view = LayoutInflater.from(mContext).inflate(R.layout.trip_view_card, parent, false);
						
						 TextView avconView = (TextView)view.findViewById(R.id.averageconsumption);
				         TextView startView = (TextView)view.findViewById(R.id.tripstart);
				         TextView avspeedView = (TextView)view.findViewById(R.id.AverageSpeed);
				         TextView trtimeView = (TextView)view.findViewById(R.id.traveltime);
				 
				         avconView.setText("~ Verbrauch: " + String.valueOf(curCard.getAvConsumption()) + " l/100km");

				         startView.setText("Fahrt gestartet am: "+curCard.getDatetime());
				         avspeedView.setText("~ " + String.valueOf(curCard.getAvSpeed()) + " km/h");

				         trtimeView.setText(String.valueOf("Dauer der Fahrt: " + curCard.getTravelTime()));
						
						break;
					//MapView
					case 2:
						view = LayoutInflater.from(mContext).inflate(R.layout.map_view_card, parent, false);
						new DownloadImageTask((ImageView) view.findViewById(R.id.imgMap)).execute(curCard.getUrl());
						
						break;						
						
					//Demo
					case 9:
						view = LayoutInflater.from(mContext).inflate(R.layout.activity_googlecards_card, parent, false);
						viewHolder.textView = (TextView) view.findViewById(R.id.activity_googlecards_card_textview);
						view.setTag(viewHolder);

						viewHolder.imageView = (ImageView) view.findViewById(R.id.activity_googlecards_card_imageview);
						viewHolder.textView.setText("This is card " + (getItem(position).getPosition() + 1));
						setImageView(viewHolder, position);
						break;
				}			
				
			} else {
				viewHolder = (ViewHolder) view.getTag();
			}			

			return view;
		}

		private void setImageView(ViewHolder viewHolder, int position) {
			int imageResId;
			switch (getItem(position).getPosition() % 5) {
			case 0:
				imageResId = R.drawable.img_nature1;
				break;
			case 1:
				imageResId = R.drawable.img_nature2;
				break;
			case 2:
				imageResId = R.drawable.img_nature3;
				break;
			case 3:
				imageResId = R.drawable.img_nature4;
				break;
			default:
				imageResId = R.drawable.img_nature5;
			}

			Bitmap bitmap = getBitmapFromMemCache(imageResId);
			if (bitmap == null) {
				bitmap = BitmapFactory.decodeResource(mContext.getResources(), imageResId);
				addBitmapToMemoryCache(imageResId, bitmap);
			}
			else
				viewHolder.imageView.setImageBitmap(bitmap);
		}

		private void addBitmapToMemoryCache(int key, Bitmap bitmap) {
			if (getBitmapFromMemCache(key) == null) {
				mMemoryCache.put(key, bitmap);
			}
		}

		private Bitmap getBitmapFromMemCache(int key) {
			return mMemoryCache.get(key);
		}

		private static class ViewHolder {
			TextView textView;
			ImageView imageView;
		}
		
		private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
			  ImageView bmImage;

			  public DownloadImageTask(ImageView bmImage) {
			      this.bmImage = bmImage;
			  }

			  protected Bitmap doInBackground(String... urls) {
			      String urldisplay = urls[0];
			      Bitmap mIcon11 = null;
			      try {
			        InputStream in = new java.net.URL(urldisplay).openStream();
			        mIcon11 = BitmapFactory.decodeStream(in);
			      } catch (Exception e) {
			          Log.e("Error", e.getMessage());
			          e.printStackTrace();
			      }
			      return mIcon11;
			  }

			  protected void onPostExecute(Bitmap result) {
			      bmImage.setImageBitmap(result);
			  }
			}
	}	
}

