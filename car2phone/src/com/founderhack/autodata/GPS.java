package com.founderhack.autodata;

import java.util.Random;

public class GPS {
	
	private static double[] latitudeArray = {31.627002, 40.353870, 51.458011, 52.203820, 78.221833, 40.570732, 40.581945, -7.476857};
	private static double[] longitudeArray = {-8.030319, -3.702393, 6.639776, 21.011353, 15.711136, -74.124842,-74.091368, -36.280975};
	
	private String latitude;
	private String longitude;

	public GPS(String latitude, String longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public static GPS getRandomGPS(){		
		Random generator = new Random(); 
		int i = generator.nextInt(8);
		
		return new GPS(Double.toString(latitudeArray[i]), Double.toString(longitudeArray[i]));
		
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}


	

	
}
