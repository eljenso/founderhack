package com.founderhack.autodata;

public class Drive {

	private String avarageSpeed;
	private String duration;
	private String avarageConsumption;
	private String started;
	private GPS parkposition;
	
	public Drive(String avarageSpeed, String duration,
			String avarageConsumption, String started, GPS parkposition) {
		super();
		this.avarageSpeed = avarageSpeed;
		this.duration = duration;
		this.avarageConsumption = avarageConsumption;
		this.started = started;
		this.parkposition = parkposition;
	}
	public String getAvarageSpeed() {
		return avarageSpeed;
	}
	public String getDuration() {
		return duration;
	}
	public String getAvarageConsumption() {
		return avarageConsumption;
	}
	public String getStarted() {
		return started;
	}
	public GPS getParkposition() {
		return parkposition;
	}
	
	
}
