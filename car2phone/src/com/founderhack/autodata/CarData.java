package com.founderhack.autodata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.os.AsyncTask;
import android.os.Environment;
import de.dsa.hackathon2013.lib.DoorStates;
import de.dsa.hackathon2013.lib.IVehicleDiagnosisApi;
import de.dsa.hackathon2013.lib.VehicleDate;
import de.dsa.hackathon2013.lib.VehicleTime;
import de.dsa.hackathon2013.lib.WindowMovingDirection;
import de.dsa.hackathon2013.VehicleDiagnosisApi;
import de.dsa.smartdiag.interpreter.provider.SmartDiagDataProviderFactory;
import de.dsa.smartdiag.vciapi.util.VciConstants;

public class CarData {

	private static final String DIRECTORY_VEHICLE_DATA = "/DSA/vehicle_data";

	private IVehicleDiagnosisApi communication; // communication object that
												// reads the data

	// Enum to switch case which data we want to poll
	private enum Data {
		TIME, DATE, FUEL_RESERVE, FUEL_NEEDLE_POSITION, VEHICLE_ID
		// TODO ADD all
	}

	// Car data
	private VehicleTime time;
	private VehicleDate date;

	private float fuelReserve;
	private float fuelNeedlePosition; // 270�C == 100%

	private de.dsa.hackathon2013.lib.FuelType FuelType; // UNKNOWN, GASOLINE,
														// METHANOL, ETHANOL,
														// DIESEL, LPG, CNG,
														// PROPANE, BATTERY,
														// BIFUEL_GASOLINE,
														// BIFUEL_MEHTHANOL,
														// BIFUEL_ETHANOL,
														// BIFUEL_LPG,
														// BIFUEL_CNG,
														// BIFUEL_PROPANE,
														// BIFUEL_BATTERY
	private float outDoorTemprature;
	private float oiltemprature;
	private float phottransistor; // brightness inside car
	private float coolantTemperature;
	private float engineRPM;
	private float vehicleSpeed;
	private float accelerationPedalPosition;
	private float range;
	private float steeringWheelPosition;
	private String vehicleID;
	private DoorStates doorStates;

	public CarData() {

	}
	
	
	public List<Drive> getListOfDrives(int numberOfElements){
		List<Drive> list = new ArrayList<Drive>();
		for(int i=0;i<numberOfElements;i++){
			list.add(getLastDrive());
		}
		return list;
	}

	// Returning an Object with random values
	public Drive getLastDrive() {
		Random generator = new Random();

		String avarageSpeed;
		String duration;
		String avarageConsumption;
		String started;
		GPS parkposition;

		int speed = generator.nextInt(160) + 30;
		avarageSpeed = speed+"";

		int hours = generator.nextInt(3);
		int minutes = generator.nextInt(59);
		duration = hours + " h and " + minutes + " min";

		int consumption = generator.nextInt(8) + 6;
		avarageConsumption = consumption + "";

		int day = generator.nextInt(20) + 1;
		int month = generator.nextInt(10) + 1;
		int year = 2013;
		started = day + "." + month + "." + year;

		parkposition = GPS.getRandomGPS();

		return new Drive(avarageSpeed, duration, avarageConsumption, started,
				parkposition);
	}

	private void configureVehicleCommunication() {
		if (communication == null) {
			// Initialise Vehicle Data from the information stored in the device
			File rootDir = Environment.getExternalStorageDirectory();
			SmartDiagDataProviderFactory.getProvider().setVehicleDatas(
					rootDir + DIRECTORY_VEHICLE_DATA + "/Passat");

			// Read vehicle data and create the communication API
			communication = new VehicleDiagnosisApi();
		}
	}

	// TODO: fix all the update methods

	public VehicleTime getTime() {
		return time;
	}

	public void updateTime() {
		time = communication.readClock();
	}

	public float getFuelReserve() {
		return fuelReserve;
	}

	public void updateFuelReserve() {
		fuelReserve = communication.readFuelTankReserve();
	}

	public float getFuelNeedlePosition() {
		return fuelNeedlePosition;
	}

	public void updateFuelNeedlePosition() {
		fuelNeedlePosition = communication.readFuelTankNeedlePosition();
	}

	public float getOutDoorTemprature() {
		return outDoorTemprature;
	}

	public void updateOutDoorTemprature() {
		outDoorTemprature = communication.readEngineOilTemperature();
	}

	public float getOiltemprature() {
		return oiltemprature;
	}

	public void updateOiltemprature() {
		oiltemprature = communication.readEngineOilTemperature();
	}

	public double getPhottransistor() {
		return phottransistor;
	}

	public void updatePhottransistor() {
		phottransistor = communication.readEnvironmentelLightValue();
	}

	public float getCoolantTemperature() {
		return coolantTemperature;
	}

	public void updateCoolantTemperature() {
		coolantTemperature = communication.readEngineCoolantTemperature();
	}

	public float getEngineRPM() {
		return engineRPM;
	}

	public void updateEngineRPM() {
		engineRPM = communication.readEngineRpm();
	}

	public float getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void updateVehicleSpeed() {
		vehicleSpeed = communication.readVehicleSpeed();
	}

	public double getAccelerationPedalPosition() {
		return accelerationPedalPosition;
	}

	public void updateAccelerationPedalPosition() {
		accelerationPedalPosition = communication
				.readAcceleratorPedalPosition();
	}

	public float getRange() {
		return range;
	}

	public void updateRange() {
		range = communication.readRange();
	}

	public String getVehicleID() {
		return vehicleID;
	}

	public void updateVehicleID() {
		new readVehicleString().execute(Data.VEHICLE_ID);
	}

	public DoorStates getDoorStatus() {
		return doorStates;
	}

	public void updateDoorStatus() {
		doorStates = communication.readDoorStates();
	}

	public de.dsa.hackathon2013.lib.FuelType getFuelType() {
		return FuelType;
	}

	public VehicleDate getDate() {
		return date;
	}

	public void saveToFileSystem() {
		// TODO
	}

	public void updateAll() {
		updateTime();
		updateFuelReserve();
		updateFuelNeedlePosition();
		updateOutDoorTemprature();
		updateOiltemprature();
		updatePhottransistor();
		updateCoolantTemperature();
		updateEngineRPM();
		updateVehicleSpeed();
		updateAccelerationPedalPosition();
		updateRange();
		updateVehicleID();
		updateDoorStatus();

	}

	public void lockDriverDoor() {
		communication.setDriverSideDoor(true);
	}

	public void unlockDriverDoor() {
		communication.setDriverSideDoor(false);
	}

	public void openDriverWindow() {
		// 4 Times to close/open the window completely on the Passat on display
		communication.moveDriverWindow(WindowMovingDirection.UP);
		communication.moveDriverWindow(WindowMovingDirection.UP);
		communication.moveDriverWindow(WindowMovingDirection.UP);
		communication.moveDriverWindow(WindowMovingDirection.UP);
		// WindowMovingDirection.DOWN
	}

	public void closeDriverWindow() {
		// 4 Times to close/open the window completely on the Passat on display
		communication.moveDriverWindow(WindowMovingDirection.DOWN);
		communication.moveDriverWindow(WindowMovingDirection.DOWN);
		communication.moveDriverWindow(WindowMovingDirection.DOWN);
		communication.moveDriverWindow(WindowMovingDirection.DOWN);
	}

	public float getSteeringWheelPosition() {
		return steeringWheelPosition;
	}

	// hack to have a tuple as return type inside our tasks
	private class Tupel<X, Y> {
		public final X x;
		public final Y y;

		public Tupel(X x, Y y) {
			this.x = x;
			this.y = y;
		}
	}

	/*
	 * 
	 * Following are a bunch of Async Tasks to get our Data from the car
	 */

	private class readVehicleString extends
			AsyncTask<Data, Void, Tupel<Data, String>> {

		public readVehicleString() {
			configureVehicleCommunication();
			VciConstants.getInstance().setVCI5Ip("192.168.126.2");
		}

		@Override
		protected Tupel<Data, String> doInBackground(Data... arg0) {
			Data desiredValue = arg0[0];
			String output;

			switch (desiredValue) {
			case VEHICLE_ID:
				output = communication.readVehicleIdentificationNumber();
				break;
			default:
				output = "";
				break;
			}

			return new Tupel<Data, String>(desiredValue, output);
		}

		@Override
		protected void onPostExecute(Tupel<Data, String> output) {
			switch (output.x) {
			case VEHICLE_ID:
				vehicleID = output.y;
				break;
			default:

				break;
			}
		}

	}

}
